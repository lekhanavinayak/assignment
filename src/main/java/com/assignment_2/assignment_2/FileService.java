package com.assignment_2.assignment_2;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FileService {
	@Autowired
	private FileDBRepository fileDBRepository;

	static final File directory = new File("C:\\Users\\lekhana.bhat\\Pictures\\images");

	public void saveImages() {
		Images images = new Images();
		int id = 1;
		BasicFileAttributes attributes = null;
		try {
			File[] f = directory.listFiles();
			for (File file : f) {
				if (file != null && (file.getName().toLowerCase().endsWith(".jpg")
						|| file.getName().toLowerCase().endsWith(".png"))) {
					Path filePath = file.toPath();
					images.setFileName(file.getName());
					images.setId(id++);
					images.setType("image");
					attributes = Files.readAttributes(filePath, BasicFileAttributes.class);
					Date creationDate = new Date(attributes.creationTime().to(TimeUnit.MILLISECONDS));
					images.setCreationDate(creationDate.toString());

				}
				fileDBRepository.save(images);
			}
		} catch (IOException exception) {
			System.out.println("Exception handled when trying to get file " + "attributes: " + exception.getMessage());
		}
	}

	public List<Images> searchImage(String type, String fileName, String creationDate, String sort) {
		List<Images> imageList = new ArrayList<>();
		if(null!=type&&!type.isEmpty()) {
			imageList = fileDBRepository.findByType(type);
		}
		if(null!=fileName&&!fileName.isEmpty()) {
			imageList = fileDBRepository.findByFileName(fileName);
		}
		if(null!=creationDate&&!creationDate.isEmpty()) {
			imageList = fileDBRepository.findByCreationDate(creationDate);
		}
		return imageList;

	}

}
