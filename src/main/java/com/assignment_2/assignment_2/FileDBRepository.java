package com.assignment_2.assignment_2;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileDBRepository extends CrudRepository<Images, Integer> 
{

	List<Images> findByType(String type);
	
	List<Images> findByFileName(String fileName);
	
	List<Images> findByCreationDate(String creationDate);
}
