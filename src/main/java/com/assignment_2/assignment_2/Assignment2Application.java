package com.assignment_2.assignment_2;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

@SpringBootApplication
public class Assignment2Application {

	public static void main(String[] args) {
		//SpringApplication.run(Assignment2Application.class, args);
		/*ApplicationContext applicationContext = SpringApplication.run(UamRbacApplication.class, args);
	    AccessChangeSchedulerService accessChangeSchedulerService = applicationContext.getBean(AccessChangeSchedulerService.class);
	    AppContext appContext = applicationContext.getBean(AppContext.class);
	    try {
	        accessChangeSchedulerService.fetchAccessLevelChanges("EP", appContext.getSplunkToken());
	    } catch (JsonProcessingException e) {
	        e.printStackTrace();
	    }
	}*/
	
		        ApplicationContext applicationContext = SpringApplication.run(Assignment2Application.class, args);
		        SomeService service = applicationContext.getBean(SomeService.class);
		        service.doSth(args);
		    }
		}

		@Service
		class SomeService {

		    public void doSth(String[] args){
		        System.out.println(Arrays.toString(args));
		    }
		}


